# Prometheus&Grafana


## Create namespace observation
~~~
kubectl create namespace observation
~~~

## Adds the Prometheus Community Helm chart repository, which contains the Prometheus Helm chart.
~~~
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
~~~

## Adds the Grafana Helm chart repository, which contains the Grafana Helm chart
~~~
helm repo add grafana https://grafana.github.io/helm-charts
~~~

## Installs the Prometheus chart, creats a Prometheus server deployment and associates resources
~~~
helm install prometheus prometheus-community/prometheus -n observation
~~~

## Updates the local cache of available Helm charts from the added repositories
~~~
helm repo update
~~~


## check the status
~~~
kubectl get all -n observation
kubectl get pods -n observation
~~~

## Exposes the Prometheus server service as a NodePort service, allows access to the Prometheus UI and API externally
~~~
kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-ext -n observation

~~~

## Installs the Grafana chart, creats a Grafana deployment and associated resources
~~~
helm install grafana grafana/grafana -n observation
~~~

## Exposes the Grafana service as a NodePort service, allows access to the Grafana UI externally
~~~
kubectl expose service grafana --type=NodePort --target-port=3000 --name=grafana-ext -n observation
~~~

## Retrieves the Grafana secret in YAML format, which contains the credentials for logging into the Grafana dashboard
~~~
kubectl get secret -n observation grafana -o yaml

~~~

## reveal the Grafana admin username & password(outputs will be the actual user&pass)
~~~
echo  user output from previous command | base64 -d ; echo
echo  password output from the previous commad | base64 -d ; echo
~~~

## Modifies the Grafana service configuration to change the target port to 3000. 
~~~
kubectl patch service grafana-ext --type=json -p='[{"op": "replace", "path": "/spec/ports/0/port", "value": 3000}]' -n observation

~~~

## Modifies the Prometheus server service configuration to change the target port to 9090
~~~
kubectl patch service prometheus-server-ext --type=json -p='[{"op": "replace", "path": "/spec/ports/0/port", "value": 9090}]' -n observation

~~~

## port forward and URL:
~~~
kubectl port-forward service/grafana 3000:3000

http://localhost:3000/
~~~

## enter data surce in Grafana and put in the Prometheus server URL:
~~~
http://prometheus-server-ext:80 
~~~

# Clean UP:
## Delete the Grafana deployment and service
~~~
helm uninstall grafana -n observation
kubectl delete service grafana-ext -n observation

~~~

## Delete the Prometheus deployment and service
~~~
helm uninstall prometheus -n observation
kubectl delete service prometheus-server-ext -n observation

~~~

## Remove the Prometheus and Grafana Helm repositories
~~~
helm repo remove prometheus-community
helm repo remove grafana

~~~
